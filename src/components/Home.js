import React, { useEffect, useState } from "react";

// Config
import { POSTER_SIZE, BACKDROP_SIZE, IMAGE_BASE_URL } from "../config";

// Components

// Hook
import { useHomeFetch } from "../hooks/useHomeFetch";

// Image
import NoImage from "../images/no_image.jpg";
import Grid from "./Grid";
import HeroImage from "./HeroImage";
import Thumb from "./Thumb";
import Spinner from "./Spinner";

const Home = () => {
    const { state, loading, error } = useHomeFetch();
    console.log("state", state);
    return (
        <>
            {state.results[0]  ?
            <HeroImage 
            image={`${IMAGE_BASE_URL}${BACKDROP_SIZE}${state.results[0].backdrop_path}`}
            title={state.results[0].title}
            text={state.results[0].overview}
            /> 
            : null
            }
            <Grid header='Popular Movies'>
                {state.results.map(movie => (
                    <Thumb 
                        key={movie.id}
                        clickable
                        image={
                            movie.poster_path ? `${IMAGE_BASE_URL}${BACKDROP_SIZE}${movie.backdrop_path}` : NoImage
                        }
                        movieId={movie.id}
                    >{movie.title}</Thumb>
                ))}
            </Grid>
            <Spinner/>
            <div>This is a main commit code</div>
        </>
    );

}

export default Home;